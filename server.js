
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());
const PORT = 3001;

function userInfo(req, res) {
  const userInfo = [
    { "userId": "1", "userName": "Makki", "userAddress": "Delhi", "userDescription": "From Delhi"},
    { "userId": "2", "userName": "Brad", "userAddress": "USA", "userDescription": "Actor"},
    { "userId": "3", "userName": "Neha", "userAddress": "Mumbai", "userDescription": "Actress"},
    { "userId": "4", "userName": "Newton", "userAddress": "Europe", "userDescription": "Scientist"},
  ]
  res.json(userInfo);
}


app.get('/api/users', userInfo);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
